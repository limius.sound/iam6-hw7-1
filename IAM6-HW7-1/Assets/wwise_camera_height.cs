﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wwise_camera_height : MonoBehaviour
{
    public GameObject Camera;
    public GameObject Player;
    public AK.Wwise.Event AmbEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        AmbEvent.Post(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        AkSoundEngine.SetRTPCValue("ext_camera_height", Camera.transform.position.y - Player.transform.position.y,
            gameObject);
    }
}
