/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AQUARIUM = 456876952U;
        static const AkUniqueID BURST_START = 2145708224U;
        static const AkUniqueID BURST_STOP = 1226353516U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID ONESHOT_PC = 1148245953U;
        static const AkUniqueID PLAY_AMB_OUTDOOR = 569015603U;
        static const AkUniqueID PLAY_BOSS = 1693071901U;
        static const AkUniqueID PLAY_DIALOGUE = 267911644U;
        static const AkUniqueID PLAY_EMPLOYEE = 2108424838U;
        static const AkUniqueID PLAY_FLIES = 1373286325U;
        static const AkUniqueID PLAY_HEARTBEAT_83_BPM = 3074184544U;
        static const AkUniqueID PLAY_ROOMTONE = 2748311587U;
        static const AkUniqueID PLAY_ROOMTONE_CAMERA_HEIGHT = 3600701587U;
        static const AkUniqueID STOP_HEARTBEAT = 3319673256U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_HEARTBEAT
        {
            static const AkUniqueID GROUP = 3555766421U;

            namespace STATE
            {
                static const AkUniqueID HEARTBEAT_START = 201282074U;
                static const AkUniqueID HEARTBEAT_STOP = 344202674U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace STATE_HEARTBEAT

        namespace STATE_INDOOR
        {
            static const AkUniqueID GROUP = 3825359022U;

            namespace STATE
            {
                static const AkUniqueID INDOOR = 340398852U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OUTDOOR = 144697359U;
            } // namespace STATE
        } // namespace STATE_INDOOR

        namespace STATE_INTRO_BATTLE_EXPLORATION
        {
            static const AkUniqueID GROUP = 3817933422U;

            namespace STATE
            {
                static const AkUniqueID BATTLE = 2937832959U;
                static const AkUniqueID EXPLORATION = 2582085496U;
                static const AkUniqueID INTRO = 1125500713U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace STATE_INTRO_BATTLE_EXPLORATION

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MUSIC_SWITCH
        {
            static const AkUniqueID GROUP = 2724869341U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_1BATTLE = 3536676683U;
                static const AkUniqueID SWITCH_2EXPLORATION = 1281636621U;
                static const AkUniqueID SWITCH_INTRO = 2618775894U;
            } // namespace SWITCH
        } // namespace MUSIC_SWITCH

        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_CARPET = 117396797U;
                static const AkUniqueID SWITCH_SURFACE_CONCRETE = 3294452117U;
                static const AkUniqueID SWITCH_SURFACE_TILE = 2754391064U;
                static const AkUniqueID SWITCH_SURFACE_WOOD = 156175339U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AMMO = 3554434311U;
        static const AkUniqueID EXT_CAMERA_HEIGHT = 1862344870U;
        static const AkUniqueID RTPC_EXT_EXPLORATION_BATTLE = 1038361083U;
        static const AkUniqueID WEAPON_ONE_SHOT_DELAY = 1425897199U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID INDOOR = 340398852U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID OUTDOOR = 144697359U;
        static const AkUniqueID WEAPON = 3893417221U;
        static const AkUniqueID WEAPON_NPC = 3889871949U;
        static const AkUniqueID WEAPON_PC = 2943625907U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID AUX_REVERB = 425396316U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
