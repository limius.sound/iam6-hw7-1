﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class wwise_footsteps : MonoBehaviour
{
    public AK.Wwise.Event footstepevent;
    private vThirdPersonInput tpInput;
    public LayerMask lm;
    private string surface;
    public GameObject leftfoot;
    public GameObject rightfoot;
    
    // Start is called before the first frame update
    void Start()
    {
        tpInput = gameObject.GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void footstep(string noga)
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            if (noga == "right")
            {
                SurfaceCheck(rightfoot);
                
            }
            else if (noga == "left")
            {
                SurfaceCheck(leftfoot);  
            }
            
            
        }
    }

    void SurfaceCheck(GameObject nogaObject)
    {
        if (Physics.Raycast(nogaObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm))
        {
            surface = hit.collider.tag;
            AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, nogaObject);
            footstepevent.Post(nogaObject);
            Debug.Log(hit.collider.tag);
        }
    }
}
