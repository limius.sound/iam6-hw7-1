﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class wwise_AIfootsteps : MonoBehaviour
{
    public AK.Wwise.Event footstepevent;
    private vControlAIMelee tpInput;
    public LayerMask lm;
    private string surface;

    // Start is called before the first frame update
    void Start()
    {
        tpInput = gameObject.GetComponent<vControlAIMelee>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footstep()
    {
        if (tpInput.input.magnitude > 0.1)
        {
            SurfaceCheck();
            AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
            footstepevent.Post(gameObject);
        }
    }

    void SurfaceCheck()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            Debug.Log(hit.collider.tag);
            surface = hit.collider.tag;
        }
    }
}
