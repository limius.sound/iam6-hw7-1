using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraplacement : MonoBehaviour
{
    public GameObject Player;
    public GameObject Camera;
    public GameObject Listener;
    

    // Update is called once per frame
    void Update()
    {
       
        Debug.DrawLine(Player.transform.position, Camera.transform.position);
        Listener.transform.position = ((Camera.transform.position - Player.transform.position) / 2) + Player.transform.position;
        Listener.transform.rotation = Camera.transform.rotation;

    }
   
}
